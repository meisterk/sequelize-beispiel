import { Sequelize, DataTypes } from 'sequelize';

// Verbindung mit Datenbank
const sequelize = new Sequelize({
    dialect: 'sqlite',
    storage: 'schule.sqlite'
});

// ===============================================
// DDL (Data Definition Language)
// Define Models
// Datenbankschema definieren
// Tabellen erzeugen
// ===============================================
const Schueler = sequelize.define('Schueler', {
    vorname: DataTypes.STRING,
    nachname: DataTypes.STRING
});

const Klasse = sequelize.define('Klasse', {
    name: DataTypes.STRING
});

const Fach = sequelize.define('Fach', {
    name: DataTypes.STRING
});

const Fach_Schueler = sequelize.define('Fach_Schueler', {
});

// Beziehung zwischen Entitätstypen: 1:n
Schueler.belongsTo(Klasse); //n:1
Klasse.hasMany(Schueler);   //1:n

// Beziehung m:n
Schueler.belongsToMany(Fach, { through: Fach_Schueler });
Fach.belongsToMany(Schueler, { through: Fach_Schueler });

// Tabellen erstellen
await Schueler.sync({ force: true });
await Klasse.sync({ force: true });
await Fach.sync({ force: true });
await Fach_Schueler.sync({ force: true });

// ===============================================
// DML (Data Manipulation Language), CRUD
// Objekte erstellen und in Datenbank speichern
// ===============================================

// CREATE
const anna = await Schueler.create({
    vorname: 'Anna',
    nachname: 'Arm'
});

const berta = await Schueler.create({
    vorname: 'Berta',
    nachname: 'Bein'
});

const carla = await Schueler.create({
    vorname: 'Carla',
    nachname: 'Copf'
});

const i1a = await Klasse.create({
    name: 'I1A'
});

const i3a = await Klasse.create({
    name: 'I3A'
});

const db = await Fach.create({
    name: 'DB'
});

const swt = await Fach.create({
    name: 'SWT'
});

// UPDATE
// Beziehungen zwischen Objekten
anna.setKlasse(i1a);
i1a.addSchueler(berta);
carla.setKlasse(i3a);

anna.addFach(db);
db.addSchueler(berta);
carla.addFach(swt);

// DELETE
//await Schueler.destroy({ where: { id: 1 } });

// READ
// Daten von Datenbank lesen
/*console.log('=== Alle Schüler ===');
const alleSchueler = await Schueler.findAll();
console.log(alleSchueler);

console.log('=== Schüler mit id 2 ===');
const schuelerMitId2 = await Schueler.findOne({ where: { id: 2 } });
console.log(`Schüler mit id 2: ${schuelerMitId2.dataValues.vorname} ${schuelerMitId2.dataValues.nachname}`);
*/

console.log('=== Schüler in I1A ===');
const schuelerInI1A = await Schueler.findAll({
    where: { KlasseId: 1 }
});
console.log(schuelerInI1A);