import { Sequelize, DataTypes } from 'sequelize';

// Verbindung mit Datenbank
//const sequelize = new Sequelize('sqlite::memory:');
const sequelize = new Sequelize({
    dialect: 'sqlite',
    storage: 'database.sqlite'
});


// Define Models
const User = sequelize.define('User', {
    username: DataTypes.STRING,
    birthday: DataTypes.DATE,
});

// Tabelle erstellen
await User.sync({ force: true });

// Persist
const jane = await User.create({
    username: 'janedoe',
    birthday: new Date('1980-06-20'),
});

const john = await User.create({
    username: 'john',
    birthday: new Date('1999-09-09'),
});

// Query
const users = await User.findAll();
console.log(users);

